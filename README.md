# SQLoadingIndicator

A description of this package.

Show indicator to cover entire screen

SQLoadingIndicatorView.show()

Show indicator to cover entire screen with custom text

SQLoadingIndicatorView.show("Loading")

Show indicator to cover target view

SQLoadingIndicatorView.show(target)

Show indicator to cover target view with custom text

SQLoadingIndicatorView.show(target, "Loading")

Hide indicator

SQLoadingIndicatorView.hide()

